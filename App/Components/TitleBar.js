import React, { Component } from 'react'
import {
   StyleSheet,
   View,
   Text
} from 'react-native'


export default class TitleBar extends Component {
   
   constructor() {
      super()
   }

   render() {
      return (
         <View>
            <Text style={styles.titleBar}>ACCUEIL</Text>
         </View>
      )
   }

}

const styles = StyleSheet.create({
   titleBar: {
      backgroundColor: '#000',
      color:  '#FFF'
   }
});

