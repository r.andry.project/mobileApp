/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Platform,
  UIManager,
} from 'react-native';


import LoginPage from '../Pages/LoginPage'
import FichePage from '../Pages/Patient/FichePage'
//import Router from '../Route/Router'
//import renderIf from '../lib/renderIf';

class LayoutFront extends Component {

  constructor(props){
    super(props);

    this.state = {
      currentUser: {},
      identifiantEntry: null,
      passwordEntry: null,
      prevPage: null,
      index: 0
    }

  }

  setUser(user) {
    this.setState({
      currentUser: user
    })
  }

  getUserId(){
    return this.state.currentUser.id;
  }

  _renderTab(pageSelected, profil = false){
    const thisService = pageSelected;
    switch(thisService){
      case "LoginPage":
      //case "Deconnexion":
        return (<LoginPage onBtnProfilPage={this.props.onServiceChange}/>);
      break;
      case "FichePage":
        return (<FichePage onBtnProfilPage={this.props.onServiceChange}/>);
      break;
    }

  }
   
  render() {

    var getUserId = null;
    var idUser = null;
    var currentUser = this.state.currentUser;
    
    if(Object.keys(currentUser).length > 0 && currentUser.hasOwnProperty("id")){
      getUserId = this.getUserId.bind(this);
      idUser = this.state.currentUser.id;
    }
    
    return (
      <View style={styles.boxBody}>
        {this._renderTab(this.props.pageSelected, this.props.profil)}
      </View>
    );

  }
  

}


const styles = StyleSheet.create({
  boxBody: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#F5FCFF',
    borderColor:  'black',
    borderWidth:  1,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  }
  
});



export default LayoutFront
