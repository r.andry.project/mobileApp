/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
//import { StackNavigator } from 'react-navigation';

//import TabNavigator from 'react-native-tab-navigator';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TextInput,
  //Navigator,
  Button,
  TouchableHighlight,
  TouchableOpacity,
  //StackNavigator,
  View
} from 'react-native';


import { createStore } from 'redux'
import { Provider } from 'react-redux'

import userReducer from './Redux/Patient/reducer'


import LayoutFront from './Layout/LayoutFront'

import * as routeApi from './Redux/routeApi'

//const store = createStore(allReducers)
const store = createStore(userReducer)

//import ConnectButton from './App/Pages/ConnectButton'

//const urlAPI = 'http://193.70.80.7/'
//const urlAPI = 'http://192.168.10.123/'
const urlAPI = 'http://192.168.10.123/'

//import * as reducers from './Route';
//const store = createStore(reducers);
//export default store;

export default class ironovaProject extends Component {

  constructor(props){
    super(props)

    this.state = {
      pageSelected: "LoginPage",
      titlePage: ''
    }

    this._switchService = this._switchService.bind(this)

  }

  _switchService(nextService){
    this.setState({ pageSelected: nextService })
    let titlePage;
    switch(nextService){
      case 'ProfilPage':
        titlePage = 'Mon profil';
      break;
     case 'EditProfilPage':
        titlePage = 'Editer mon profil';
      break;
      default:
        titlePage = '';
      break;
    }
    this.setState({ titlePage: titlePage })
  }

  componentWillMount_(){
    console.log("-componentWillMount-");

    var params = "action=getUserLogin&identifiant=julia&password=password";

    //fetch("http://mobileapi.ironova.com/getUser", {
    fetch(routeApi.GETUSERPROFIL, {
      method  : 'POST',
      headers: {
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
      },
      //body: "action=getUserLogin&identifiant=julia&password=password"
      body: params
    }).then((response) => response.json())
    .then((responseJSON) => {
        //console.log("-responseJSON-", responseJSON);
        //console.log("-responseJSON-", typeof responseJSON);
        if(JSON.stringify(responseJSON) == "0"){
          alert("Veuillez entrer un identifiant et un mot de passe !");
        }
        else if(JSON.stringify(responseJSON) == "null"){
          alert("Utilisateur non identifié, veuillez réessayer !");
        }else{
          var userProfil = responseJSON;
          if(typeof userProfil == "object" && userProfil.hasOwnProperty("userData")){
            console.log(userProfil);
            //this.state.currentUser = userProfil.userData;
            //if(userProfil.userData.role == "patient")
              //this.goToHome(userProfil.userData.id);
          }else{
            alert("Une erreur est survenue, veuillez réessayer !");
          }
        }
    }).catch((error) => {
      console.warn(error);
    });

  }
  

  render() {
    return (
      <Provider store={store}>
        <LayoutFront
          pageSelected={this.state.pageSelected}
          titlePage={this.state.titlePage}
          onServiceChange={this._switchService}
        />
      </Provider>
    );
  }

}

const styles = StyleSheet.create({
   navigationBar: {
  	backgroundColor: 'blue',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  textInput: {
    height: 80,
    fontSize: 30,
    backgroundColor: '#FFF'
  },
  buttonSubmit: {
    color: "#2F61F8"
  },
  navBarMainTop: {
    backgroundColor: '#2F61F8'
  },
     navigationBar: {
      backgroundColor: 'blue',
   },
   leftButton: {
      color: '#ffffff',
      margin: 10,
      fontSize: 17,
   },
   title: {
      paddingVertical: 10,
      color: '#ffffff',
      justifyContent: 'center',
      fontSize: 18
   },
   rightButton: {
      color: 'white',
      margin: 10,
      fontSize: 16
   }
});

AppRegistry.registerComponent('ironovaProject', () => ironovaProject);
