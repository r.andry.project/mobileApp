import React, { Component } from 'react'

import {
   StyleSheet,
   Text,
   View,
   Navigator,
   //NavigationExperimental,
   TouchableOpacity
} from 'react-native'

const {
  CardStack: NavigationCardStack
} = NavigationExperimental

import LoginPage from '../Pages/LoginPage'
import PatientFichePage from '../Pages/Patient/FichePage'

import TitleBar from '../Components/TitleBar'

export default class Router extends Component {

   constructor(){
      super()

      this.state = {
        idUser: null,
        setUser: null,
        getUserId: null,
        prevPage: "login"
      }

   }

   render() {
      console.log("-ROUTER-");
      console.log(this.props.currentUserId);
      
      /*
      this.setState({
         setUser: this.props.getUserId
      })
      */

      if(this.props.currentUserId != null){
         this.setState({
            idUser: this.props.currentUserId,
            getUserId: this.props.getUserId
         })
      }

      return (

         <Navigator
            initialRoute = {{ name: 'Login', title: 'Login' }}
            renderScene = { this.renderScene.bind(this) }
            navigationBar = {
                  <Navigator.NavigationBar
                     style = { styles.navigationBar }
                     routeMapper = { NavigationBarRouteMapper } />
                     
            }

         />
      );
   }

   renderScene(route, navigator) {
      console.log("-renderScene-", route);
      console.log("-renderScene-", route.idUser);
      //var a = 123;
      if(route.name == 'Login' || route.name == 'Logout'){
         return (
            <View>
               <LoginPage
                  setUser={this.state.setUser}
                  navigator = {navigator}
                  {...route.passProps} 
                  
               />
            </View>
         )
      }
      else if(route.name == 'AccueilPatient'){
         return (
            <PatientFichePage
               //getUserId={this.state.getUserId}
               getUserId = {route.idUser}
               getCurrentUser = {route.currentUser}
               navigator = {navigator}
               {...route.passProps} 
            />
         )
      }
   }

}

var NavigationBarRouteMapper = {
   LeftButton(route, navigator, index, navState) {
      console.log("-navState-");
      //console.log(navState);
      //console.log(route);
      //console.log(navigator);
      
      var backButton = <View>
                           <TouchableOpacity
                              onPress = {() => { if (index > 0) { navigator.pop() } }}>
                              <Text style={styles.title}>
                                 Back
                              </Text>
                           </TouchableOpacity>
                        </View>;

      if(index > 0) {
      //if(this.state.prevPage != "Login"){

         return (
            <View>
               {backButton}
            </View>
         )
      }
      else { return null }
   },
   displayMenu: function(route, navigator, index, navState) {
      alert("-displayMenu-");
   },
   RightButton(route, navigator, index, navState) {
      //console.log(route);
      var deconnexionButton = <View style={styles.leftContainer}>
                                 <TouchableOpacity
                                    onPress = {() => { navigator.push({ "name": "Logout", title: null }); }}>
                                    <Text style={styles.title}>
                                       Déconnexion
                                    </Text>
                                 </TouchableOpacity>
                              </View>;
      var menuButton = <View style={styles.leftContainer}>
               <TouchableOpacity
                  onPress = { this.displayMenu }>
                  <Text style = { styles.rightButton }>
                     { route.rightText || 'Menu' }
                  </Text>
               </TouchableOpacity>
            </View>
      
      switch(route.name){
         case "Login":
            deconnexionButton = null;
            menuButton = null;
            break;
         //default:
      }
      if (route.openMenu) return (
         <View style={styles.textContainer}>
            {deconnexionButton}
            {menuButton}
         </View>
      )
   },
   Title(route, navigator, index, navState) {
      /*
      return (
         <Text style = { styles.title }>
            {route.title}
         </Text>
      )
      */
      
   }

};

const styles = StyleSheet.create({
   textContainer:{
      flexDirection: 'row',
      height: "100%",
      //backgroundColor: 'green'
   },
   navigationBar: {
      alignItems: 'center',
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-start',
      backgroundColor: 'blue',
      borderColor:  'red',
      borderWidth:  1,
   },
   leftButton: {
      color: '#ffffff',
      margin: 10,
      fontSize: 17,
   },
   title: {
      paddingVertical: 10,
      color: '#ffffff',
      justifyContent: 'center',
      fontSize: 18
   },
   rightButton: {
      color: 'white',
      margin: 10,
      fontSize: 16
   },
   titleBar: {
      backgroundColor: '#000',
      color:  '#FFF',
      borderColor:  'red',
      borderWidth:  1,
   },
  rightContainer:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    backgroundColor: 'blue',
  },
   leftContainer:{
      flex: 1,
      justifyContent: 'center',
      alignItems:'center',
      //backgroundColor: 'red',
   }

})