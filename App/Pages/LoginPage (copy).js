/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';

import LoginButton from '../Components/Button/LoginButton'
const urlAPI = 'http://192.168.10.123/'

class LoginPage extends Component {

  constructor(){
    super();
    console.log("LOGIN PAGE");
    this.state = {
      currentUser: {},
      identifiantEntry: null,
      passwordEntry: null
    }

  }

  setUser(user) {
    // Load data
    this.state.currentUser(user);
  }

  render(){
    return(
      <View>
        <View>
          <Text>ANKKORO</Text>
        </View>
        <View>
          <Text>Identifiant</Text>
        </View>
        <View>
          <TextInput value={this.state.identifiantEntry} autoFocus={true} onChangeText={identifiantEntry => this.setState({identifiantEntry})} name="identifiant" style={styles.textInput} />
        </View> 
        <View>
          <Text>Mot de passe</Text>
        </View>
        <View>
          <TextInput value={this.state.passwordEntry} secureTextEntry={true} onChangeText={passwordEntry => this.setState({passwordEntry})} name="password" style={styles.textInput} />
        </View>
        <View>
          <LoginButton goToHome = {this.login}/>
        </View>
      </View>
    );
  }

  login = () => {

    console.log("-pressLogin-");
    console.log(this.state.identifiantEntry);
    console.log(this.state.passwordEntry);

    var identifiant = this.state.identifiantEntry;
    var password = this.state.passwordEntry;
    var params = "action=getUserLogin&identifiant="+identifiant+"&password="+password;


    var request = new XMLHttpRequest();
    
    request.onreadystatechange = (e) => {
      e.preventDefault();
      if(request.readyState !== 4){
        return;
      }

      if(request.status === 200){
        if(request.responseText == "0"){
          alert("Veuillez entrer un identifiant et un mot de passe !");
        }
        else if(request.responseText == "null"){
          alert("Utilisateur non identifié, veuillez réessayer !");
        }else{
          var userProfil = JSON.parse(request.response);
          if(typeof userProfil == "object" && userProfil.hasOwnProperty("userData")){
            
            this.state.currentUser = userProfil.userData;
            if(userProfil.userData.role == "patient")
              this.goToHome(userProfil.userData.id);
          }else{
            alert("Une erreur est survenue, veuillez réessayer !");
          }
        }
        request = null;
      }else{
        console.warn('error');
        request = null;
      }

    };

   
    request.open('POST', 'http://mobileapi.ironova.com/getUser');
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded;charset=UTF-8");
    request.setRequestHeader("Content-length", params.length);
    request.send(params);
    request.setRequestHeader("Connection", "close");

  }

  openMenu = () =>{
    alert("Menu button pressed!");
  }
  
  goToHome = (id) => {
    console.log("-redirect to patient fiche-");
    var homePage;
    if(this.state.currentUser.role == "patient")
      homePage = "AccueilPatient";
    else if(this.state.currentUser.role == "practicien")
      homePage = "AccueilPractient";
    else
      homePage = "Logout";

    this.props.navigator.push({
       'name': homePage,
       'title': 'Accueil',
       'openMenu': this.openMenu,
       'idUser': id,
       'currentUser': this.state.currentUser
    });
  }


}

const styles = StyleSheet.create({
  map: {
    flex: 1,
    marginTop: 64
  },
  container: {
    alignItems: 'center',
    backgroundColor: '#F5FCFF'  
  },
  img: {
    alignItems: 'center',
    width: 200,
    height: 200
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center', 
    margin: 10,
  },
  btnText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff'
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#859a9b',
    borderRadius: 15,
    padding: 10
  },
  textInput: {
    alignItems: 'center',
    height: 80,
    fontSize: 30,
    backgroundColor: '#FFF'
  }
});

export default LoginPage
