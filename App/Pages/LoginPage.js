/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';

import LoginButton from '../Components/Button/LoginButton'
const urlAPI = 'http://192.168.10.123/'

class LoginPage extends Component {

  constructor(){
    super();
    console.log("LOGIN PAGE");
    this.state = {
      currentUser: {},
      identifiantEntry: null,
      passwordEntry: null
    }

  }

  setUser(user) {
    // Load data
    this.state.currentUser(user);
  }

  render(){
    return(
      <View>
        <View>
          <Text>ANKKORO</Text>
        </View>
        <View>
          <Text>Identifiant</Text>
        </View>
        <View>
          <TextInput value={this.state.identifiantEntry} autoFocus={true} onChangeText={identifiantEntry => this.setState({identifiantEntry})} name="identifiant" style={styles.textInput} />
        </View> 
        <View>
          <Text>Mot de passe</Text>
        </View>
        <View>
          <TextInput value={this.state.passwordEntry} secureTextEntry={true} onChangeText={passwordEntry => this.setState({passwordEntry})} name="password" style={styles.textInput} />
        </View>
        <View>
          <TouchableOpacity onPress={comp => {this.props.onBtnProfilPage("FichePage")}} style={styles.button}>
            <Text style={styles.btnText}>Se CONNECTER</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  login = () => {

  }

  openMenu = () =>{
    alert("Menu button pressed!");
  }
  
  goToHome = (id) => {
    console.log("-redirect to patient fiche-");
    var homePage;
    if(this.state.currentUser.role == "patient")
      homePage = "AccueilPatient";
    else if(this.state.currentUser.role == "practicien")
      homePage = "AccueilPractient";
    else
      homePage = "Logout";

    this.props.navigator.push({
       'name': homePage,
       'title': 'Accueil',
       'openMenu': this.openMenu,
       'idUser': id,
       'currentUser': this.state.currentUser
    });
  }


}

const styles = StyleSheet.create({
  map: {
    flex: 1,
    marginTop: 64
  },
  container: {
    alignItems: 'center',
    backgroundColor: '#F5FCFF'  
  },
  img: {
    alignItems: 'center',
    width: 200,
    height: 200
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center', 
    margin: 10,
  },
  btnText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff'
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#859a9b',
    borderRadius: 15,
    padding: 10
  },
  textInput: {
    alignItems: 'center',
    height: 80,
    fontSize: 30,
    backgroundColor: '#FFF'
  }
});

export default LoginPage
