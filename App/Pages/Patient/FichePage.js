import React, { Component } from 'react'
import {
   View,
   Text,
   TouchableOpacity,
   StyleSheet,
   Image,
   Dimensions,
} from 'react-native'

//import LoginPageButton from '../../Components/Button/LoginPageButton'

export default class FichePage extends Component {
   constructor() {
      super()

      this.state = {
         idUser: null,
         profilUser: {},
         currentUser: {}
      }

   }
   
   render() {
      return (
          <View style={styles.boxFiche}>
            <View><Text>Fiche </Text></View>
            <TouchableOpacity onPress={comp => {this.props.onBtnProfilPage("LoginPage")}} style={styles.button}>
              <Text>DECONNEXION</Text>
            </TouchableOpacity>
          </View>

      )
   }

}

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const styles = StyleSheet.create({
   boxFiche: {
    marginTop: 100
  },
  profilPageContainer: {
    flex: 1,
    width: width,
    height: height,
    flexDirection: 'column',
    //alignItems: 'flex-start',
    backgroundColor: '#F5FCFF'
  },
  profilPageblockInfo: {
    flex: 1,
    width: width,
    height: height,
    flexDirection: 'row',
    alignItems: 'flex-start',
    backgroundColor: '#F5FCFF'
  },
   profilPageValue:{
      fontSize: 50
   },
   profilPageItemContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    backgroundColor: '#F5FCFF'
  },
  profilPageItem: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  profilPageItemIcon: {
    alignItems: 'center'
  },
  
  blockPicture: {
    flex: 1,
    justifyContent: 'center'
  },
  blockInfo: {
    flex: 1
  },
  child: {
    fontSize: 20,
    color: "#000"
  },
  img: {
    alignItems: 'center',
    width: 200,
    height: 200
  },
  tabIcon: {
    fontSize: 100,
    color: '#000'
  },

});
