import { combineReducers } from 'redux';

//import appReducer from './appReducer'
import userReducer from './reducer'

const allReducers = combineReducers({
	//app : appReducer,
	user : userReducer
})

export default allReducers