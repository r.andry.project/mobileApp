import { connect } from 'react-redux'

import * as patientActions from './actions'
import * as routeApi from '../routeApi'

import LoginPage from '../../Pages/LoginPage'


const mapStateToProps = (state) => {
	return {
		state: {
			//app : state.app,
			patientProfil : state.profil,
			patientFeatures : state.features
		}
	}
}

const mapDispatchToProps = (dispatch) => {

	return {
		getPatient : (params) => {
			console.log("-getPatient-");

		    fetch(routeApi.GETUSERPROFIL, {
		      method  : 'POST',
		      headers: {
		        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
		      },
		      //body: "action=getUserLogin&identifiant=julia&password=password"
		      body: params
		    }).then((response) => response.json())
		    .then((responseJSON) => {
		        //console.log("-responseJSON-", responseJSON);
		        //console.log("-responseJSON-", typeof responseJSON);
		        if(JSON.stringify(responseJSON) == "0"){
		          alert("Veuillez entrer un identifiant et un mot de passe !");
		        }
		        else if(JSON.stringify(responseJSON) == "null"){
		          alert("Utilisateur non identifié, veuillez réessayer !");
		        }else{
		          var userProfil = responseJSON;
		          if(typeof userProfil == "object" && userProfil.hasOwnProperty("userData")){
		            console.log(userProfil);
		            if(userProfil.userData.role == "patient")
		            	dispatch(patientActions.setUserProfil(userProfil.userData))
		          }else{
		            alert("Une erreur est survenue, veuillez réessayer !");
		          }
		        }
		    }).catch((error) => {
		      console.warn(error);
		    });

		},
		getPatientFeatures : (id) => {
			console.log("-getPatientFeatures-");

		    fetch(routeApi.GETUSERFEATURES, {
		      method  : 'POST',
		      headers: {
		        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
		      },
		      //body: 'action=getUserLogin&identifiant=julias&password=password'
		      body: "action=getProfilUser&idUser="+id
		    }).then((response) => response.json())
		    .then((responseJSON) => {
	           if(request.responseText == "null"){
	           		alert("Utilisateur non identifié, veuillez réessayer !");
	           }else{
	             var userProfil = JSON.parse(request.response);
	             if(typeof userProfil == "object" && userProfil.hasOwnProperty("userData")){		               
	                dispatch(patientActions.setUserFeatures(userProfil.userData));
	             }else{
	               alert("Une erreur est survenue, veuillez réessayer !");
	             }
	           }
		    }).catch((error) => {
		      console.warn(error);
		    });

		}
	}


}

const PatientContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(LoginPage)

export default PatientContainer


