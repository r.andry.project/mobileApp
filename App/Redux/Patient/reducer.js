// import {LOAD_BOOKMARKS} from './actions'
import * as type from './actionTypes'

const initialState = {
	//bookmarks : []
	user : {},
	features : {}
};

export default function reducer(state = initialState, action){
	switch(action.type){
		//case LOAD_BOOKMARKS:
		case type.SETUSERPROFIL:
			return {
				...state,
				//bookmarks: [...action.payload],
				user: action.data.user,
			}
		case type.SETUSERFEATURES:
			return {
				...state,
				features: action.data.features,
			}
		default:
			return state;
	}
}