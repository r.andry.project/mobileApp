import * as types from './actionTypes'

//const SET_USERPROFIL = 'SET_USERPROFIL';
//const SET_USERFEATURES = 'SET_USERFEATURES';

//export function loadBookmarks(bookmark){	
export function setUserProfil(user){
	return	{
		type: SETUSERPROFIL,
		//payload: [
		data: {user: user}
	};
}

export function setUserFeatures(features){
	return	{
		type: SETUSERFEATURES,
		//payload: [
		data: {features: features}
	};
}

