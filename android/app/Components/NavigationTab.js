/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { PropTypes } from 'react';
import {
  StyleSheet
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome'
import Tabs from 'react-native-tabs'
import TabItem from '../Components/TabItem'

const btnTabs = (props) => (
 <Tabs style={styles.boxNavigationTabs}
    onSelect={comp => { props.onTabChange(comp.props.name) }}
  >
    <TabItem name="HomePage" page="HomePage" icon="home" label="Accueil"/>   
    <TabItem name="ProfilPage" page="ProfilPage" icon="users" label="Mes amis"/>   
    <TabItem name="ProfilPage" page="ProfilPage" icon="trophy" label="Mes trophés"/>   
  </Tabs>
)

btnTabs.propTypes = {
  onTabChange: PropTypes.func.isRequired
}

const styles = StyleSheet.create({
  boxNavigationTabs: {
    height:  100,
    backgroundColor: '#343434'
  },
});


export default btnTabs
