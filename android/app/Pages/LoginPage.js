/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';

//import Icon from 'react-native-vector-icons/FontAwesome'

class LoginPage extends Component {

  render(){
    return(
      <View>
        <View>
          <Text>ANKKORO</Text>
        </View>
        <View>
          <Text>Identifiant</Text>
        </View>
        <View>
          <TextInput name="identifiant" style={styles.textInput} />
        </View> 
        <View>
          <Text>Mot de passe</Text>
        </View>
        <View>
          <TextInput name="passsword" style={styles.textInput} />
        </View>
        <View>
          <Button title="Se connecter" color="#2F61F8" style={styles.textInput} />
        </View>
      </View> 
    );
  }
}

const styles = StyleSheet.create({
  map: {
    flex: 1,
    marginTop: 64
  },
  container: {
    alignItems: 'center',
    backgroundColor: '#F5FCFF'  
  },
  img: {
    alignItems: 'center',
    width: 200,
    height: 200
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center', 
    margin: 10,
  },
  btnText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff'
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#859a9b',
    borderRadius: 15,
    padding: 10
  },
  textInput: {
    height: 80,
    fontSize: 30,
    backgroundColor: '#FFF'
  }
});

export default LoginPage
